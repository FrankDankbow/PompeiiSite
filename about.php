<!DOCTYPE html>
<html lang="en">

<!-- Title -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		
	<title>About / Pompeii.net</title>

	<!-- CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

<body>
	<?php
		require "footer.php";
		require "navbar.php";
		
		$COLOR = "red";
		
		navbar($COLOR, "about.php");
	?>
	
	<div id="index-banner" class="parallax-container row tooltipped" data-position="bottom" data-tooltip="'The Last Day of Pompeii' by Karl Bryullov">
		<div class="section no-pad-bot">
			<div class="container">
				<br><br>
				<h1  class="header center white-text">What Is Pompeii.net?</h1>
				<div class="row center">
					<h5  class="header col s12 white-text">Lorem ipsum?</h5>
				</div>
				<br><br>
			</div>
		</div>
		<div class="parallax">
			<img src="/images/the_last_day_of_pompeii.jpg" alt="'The Last Day of Pompeii' by Karl Bryullov">
		</div>
	</div>

	<div class="container">
		<div class="section">
			<div class="row">
				<div class="col s12 m12">
					<div class="icon-block">
						<h5 class="center">Pompeii.net</h5>
						<p class="light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id ex vestibulum, volutpat lorem gravida, lacinia massa. Suspendisse sem nibh, interdum ut dolor interdum, lacinia tristique erat. Nunc hendrerit placerat eros eget finibus. Maecenas hendrerit fringilla eros, eu varius diam bibendum et. Proin arcu leo, semper et viverra in, elementum id quam. Pellentesque placerat sagittis mauris, sit amet posuere libero varius ut. Maecenas et tempor sem, quis tincidunt tellus. In eu faucibus libero, eu lacinia elit. Maecenas lacus nisi, commodo sed sapien in, faucibus gravida nunc.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m6">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons medium">search</i></h2>
						<div class="row center">
							<a href="/search" id="download-button" class="btn-large waves-effect waves-light red lighten-1 z-depth-5">Search Through Pompeii.net's Archives</a>
						</div>
					</div>
				</div>
				<div class="col s12 m6">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons medium">dns</i></h2>
						<div class="row center">
							<a href="/forum" id="download-button" class="btn-large waves-effect waves-light red lighten-1 z-depth-5">Browse The Archives Directly</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php
		footer($COLOR);
	?>
	
	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="/js/materialize.js"></script>
	<script src="/js/init.js"></script>
	</body>
</html>
