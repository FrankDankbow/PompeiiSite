<!DOCTYPE html>
<html lang="en">

<?php
	require "footer.php";
	require "navbar.php";
	
	// Connect
	$servername = "localhost";
	$username = "pompeii";
	$password = "password";
	$dbname = "PompeiiNet";
	$DBCONNECTION = mysqli_connect($servername, $username, $password, $dbname);
	
	// Check connection
	if (!$DBCONNECTION) {
		die("Connection failed: " . mysqli_connect_error());
	}
	
	$POSTPERPAGE=10;
	
	# Sets the variable so PHP doesn't give a warning
	$PAGE = 1;
	
	// TODO: sanitize _GET functions
	if( isset($_GET["page"])){
		$PAGE = $_GET["page"];
	}
	
	if( isset($_GET["act"])){
		$ACTID = $_GET["act"];
	}
	
	if( isset($_GET["search"])){
		$SEARCHTERM = $_GET["search"];
	}

	$GLOBALS['ACTID'] = $ACTID;
	$GLOBALS['SEARCH_TERM'] = $SEARCHTERM;
	
	// Default to page 1
	if ($PAGE < 1){
		$PAGE = 1;
	}

	// Get the number of pages of search results
	$RESULT = $DBCONNECTION->query("SELECT CEIL(COUNT(post_id)/$POSTPERPAGE) AS page_count FROM post_table NATURAL JOIN scene_table NATURAL JOIN act_table WHERE act_id = $ACTID AND MATCH (post_content) AGAINST ('$SEARCHTERM' IN NATURAL LANGUAGE MODE)");
	$NUMPAGES = $RESULT->fetch_assoc();
	$NUMPAGES = $NUMPAGES['page_count'];
	$RESULT->free_result();
	
	// Default to last page
	if ($PAGE > $NUMPAGES){
		$PAGE = $NUMPAGES;
	}
	
	// Get the number of posts on this page
	$OFFSET = $POSTPERPAGE*($PAGE-1);
	$RESULT = $DBCONNECTION->query("SELECT * FROM post_table NATURAL JOIN user_table NATURAL JOIN scene_table WHERE act_id = $ACTID AND MATCH (post_content) AGAINST ('$SEARCHTERM' IN NATURAL LANGUAGE MODE) ORDER BY post_id LIMIT $POSTPERPAGE OFFSET $OFFSET");
	$NUMROWS = $RESULT->num_rows;
	
	$COLOR = "red"; // Color is the accent color
	$GLOBALS['COLOR'] = $COLOR;
	$BODYCOLOR="white";
	
	// Function to create UI for changing pages
	function page_picker($PAGE, $NUMPAGES) {
		$OPTIONCOUNT=8;
		$NEXTPAGE=($PAGE+1);
		$PREVPAGE=($PAGE+(-1));

		if ($PAGE <= 1) {
			echo "<li class=\"disabled z-depth-1\"><a href=\"#!\"><i class=\"material-icons\">arrow_back</i></a></li>";
			echo "<li class=\"disabled z-depth-1\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>";
		} else {
			echo "<li class=\"waves-effect z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] ."&page=1\"><i class=\"material-icons\">arrow_back</i></a></li>";
			echo "<li class=\"waves-effect z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] . "&page=$PREVPAGE\"><i class=\"material-icons\">chevron_left</i></a></li>";

			if ($PAGE > ($NUMPAGES - $OPTIONCOUNT)) {
				$ITERATOR=($NUMPAGES - $OPTIONCOUNT);
			} else {
				$ITERATOR=($PAGE - 4);
			}
			
			if($ITERATOR < 1){
				$ITERATOR=1;
			}
			
			while($ITERATOR < $PAGE){
				echo "<li class=\"waves-effect z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] . "&page=$ITERATOR\">$ITERATOR</a></li>";
				$OPTIONCOUNT=($OPTIONCOUNT - 1);
				$ITERATOR=($ITERATOR + 1);
			}
		}
	
		echo "<li class=\"active waves-effect " . $GLOBALS['COLOR'] . " z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] . "&page=$PAGE\">$PAGE</a></li>";
	
		if ($PAGE >= $NUMPAGES) {
			echo "<li class=\"disabled z-depth-1\"><a href=\"#!\"><i class=\"material-icons\">chevron_right</i></a></li>";
			echo "<li class=\"disabled z-depth-1\"><a href=\"#!\"><i class=\"material-icons\">arrow_forward</i></a></li>";
		} else {
			$ITERATOR=$PAGE;
			$MAX=($ITERATOR + $OPTIONCOUNT);

			if($MAX > $NUMPAGES){
				$MAX=$NUMPAGES;
			}
			
			while($ITERATOR < $MAX){
				$ITERATOR=($ITERATOR + 1);
				echo "<li class=\"waves-effect z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] . "&page=$ITERATOR\">$ITERATOR</a></li>";
			}
			
			echo "<li class=\"waves-effect z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] . "&page=$NEXTPAGE\"><i class=\"material-icons\">chevron_right</i></a></li>";
			echo "<li class=\"waves-effect z-depth-1\"><a href=\"searchfor.php?search=" . $GLOBALS['SEARCH_TERM'] . "&act=" . $GLOBALS['ACTID'] . "&page=$NUMPAGES\"><i class=\"material-icons\">arrow_forward</i></a></li>";
		}
		
		// Jump to page X based on user input
		echo "\n<div class=\"row\">";
		echo "\n\t<form action=\"searchfor.php?\" method=\"get\">";
		echo "\n\t<div class=\"input-field col s4 m4 l4 offset-s4 offset-m4 offset-l4\">";
		echo "\n\t\t<input type=\"hidden\" id=\"search\" name=\"search\" value=\"" . $GLOBALS['SEARCH_TERM'] . "\" />";
		echo "\n\t\t<input type=\"hidden\" id=\"act\" name=\"act\" value=\"" . $GLOBALS['ACTID'] . "\" />";
		echo "\n\t\t<input id=\"page\" name=\"page\" type=\"text\" class=\"validate\">";
		echo "\n\t\t<label for=\"page\">Jump To Page #</label>";
		echo "\n\t</div>";
		echo"\n\t</form>\n</div>";

		return;
	}
	?>

<!-- Title -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	
	<?php
		echo "\t<title>Search for '$SEARCHTERM' / Pompeii.net</title>";
	?>
	
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>


<?php
	echo "<body class=\"$BODYCOLOR\">";
	navbar($COLOR, "search.php");
?>

	<div id="index-banner" class="parallax-container row">
		<div class="section no-pad-bot">
			<div class="container">
				<br><br><br><br>
				
				<?php
					echo "\n<h1 class=\"header center text-lighten-2 white-text\"><strong>Search for '$SEARCHTERM'</strong></h1>";
				?>
				<div class="row center">
				</div>
			</div>
		</div>
		<div class="parallax">
			<img src="images/the_last_day_of_pompeii.jpg" alt="Default banner">
		</div>
	</div>

	<div class="container">
		<ul class="pagination center">
			<?php 
				page_picker($PAGE, $NUMPAGES);
			?>
		</ul>
		
		<div class="section">	
			<div class="row">
				<div class="col s12 m12">
	
	<div class="card white">
			<?php 
			// Output all the posts on the page
			if ($NUMROWS > 0) {
				echo "\n<div class=\"card-action white darken-1\">";
				
				while($row = $RESULT->fetch_assoc()) {
					echo "\n<ul class=\"collection\">";
					echo "\n<li class=\"collection-item\">";
					echo "\n<div class=\"chip btn waves-effect waves-light modal-trigger z-depth-2\" href=\"#profile-" . $row["user_id"] . "\">";
					echo $row["username_current"];
					echo "\n</div>";
					echo "\n<div class=\"chip\">";
					echo "\nPosted " . $row["post_date"] . " UTC";
					echo "\n</div>";
					echo "\n<div class=\"chip\">";
					echo "\n" . $row["scene_name"];
					echo "\n</div>";
					echo "\n</li>";
					
					echo "\n<li class=\"collection-item\">";
					echo "<p class=\"flow-text\">";
					echo trim(stripslashes($row["post_content"]), "'");
					echo "</p>";
					echo "\n</li>";
					
					if ( ! $row["edit_date"] === NULL ){
						echo "\n<li class=\"collection-item\">";
					
						echo "\n<div class=\"chip\">";
						echo "Edited on " . $row["edit_date"] . " by " . $row["editor"];
						echo "\n</div>";
					
						// If edit reason add edit reason chip
						if ( $row["edit_reason"] === NULL ){
							
						} else {
							echo "\n<div class=\"chip\">";
							echo $row["edit_reason"];
							echo "\n</div>";
						}
					
						echo "\n</li>";	
					}
					echo "\n</ul>";
				}
				echo "\n</ul>";	
			} else {	
				echo "DATABASE ERROR: 0 RESULTS";
			}
			
			?>
	</div>

	<!-- Page picker on the bottom -->
	<ul class="pagination center">
		<?php 
			page_picker($PAGE, $NUMPAGES);
		?>
	</ul>
	
	<!-- Profile Modals -->
	<?php	
		$PROFRESULT = $DBCONNECTION->query("SELECT DISTINCT user_id,username_current,join_date FROM (SELECT * FROM post_table NATURAL JOIN user_table NATURAL JOIN scene_table WHERE act_id = $ACTID AND MATCH (post_content) AGAINST ('$SEARCHTERM' IN NATURAL LANGUAGE MODE) ORDER BY post_id LIMIT $POSTPERPAGE OFFSET $OFFSET) as page");
		$PROFROWS = $PROFRESULT->num_rows;
		
		if ($PROFROWS > 0) {
			while($PROFROW = $PROFRESULT->fetch_assoc()) {
				echo "\n<div id=\"profile-" . $PROFROW["user_id"] . "\" class=\"modal\">";
				echo "\n\t<div class=\"modal-content\">";
				
				echo "\n\t\t<h3>" . $PROFROW["username_current"] . "</h3>";
				echo "\n\t\t<p>Join date: " . $PROFROW["join_date"] . "</p>";
				
				// Get the post count for each user
				$PROFCOUNT = $DBCONNECTION->query("SELECT COUNT(post_id) AS postcount FROM post_table WHERE user_id = " . $PROFROW["user_id"]);
				$COUNTROW = $PROFCOUNT->fetch_assoc();
				$POSTCOUNT = $COUNTROW["postcount"];
				$PROFCOUNT->free_result();
				
				echo "\n\t\t<p>Posts by this user: $POSTCOUNT</p>";
				echo "\n\t</div>";
				echo "\n\t<div class=\"modal-footer\">";
				echo "\n\t\t<a href=\"#!\" class=\"modal-close waves-effect waves-green btn-flat\">Close</a>";
				echo "\n\t</div>";
				echo "\n</div>\n";
			}
		}
	?>
	
	<!-- Footer -->
	<?php
		// Close all DB Connections
		mysqli_close($DBCONNECTION);
		
		footer($COLOR);
	?>
	
	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>
	</body>
</html>
