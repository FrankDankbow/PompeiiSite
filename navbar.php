<?php
	function navlist($COLOR, $ACTIVE){
		$NOTIFICATIONCOLOR = "green";
		
		if ($ACTIVE == "index.php"){
			echo "\n<li class=\"active\"><a href=\"#!\">";
		} else {
			echo "\n<li><a href=\"/\">";
		}
		echo "<i class=\"material-icons left\">home</i>Home</a></li>";
		
		if ($ACTIVE == "about.php"){
			echo "\n<li class=\"active\"><a href=\"#!\">";
		} else {
			echo "\n<li><a href=\"/about\">";
		}
		echo "<i class=\"material-icons left\">library_books</i>About Us</a></li>";
		
		if ($ACTIVE == "search.php"){
			echo "\n<li class=\"active\">";
		} else {
			echo "\n<li>";
		}
		echo "<a href=\"/search\"><i class=\"material-icons left\">search</i>Search<span class=\"new badge $NOTIFICATIONCOLOR\" data-badge-caption=\"NEW\"></span></a></li>";
		
		if ($ACTIVE == "forum.php"){
			echo "\n<li class=\"active\">";
		} else {
			echo "\n<li>";
		}
		echo "<a href=\"/forum\"><i class=\"material-icons left\">dns</i>Forum</a></li>";
	}

	function navbar($COLOR, $ACTIVE){
		echo "\n<!-- Navbar -->";
		echo "\n<div class=\"navbar-fixed\">";
			
		echo "\n\t<nav class=\"$COLOR z-depth-5\" role=\"navigation\">";

		echo "\n\t\t<div class=\"nav-wrapper container\">";
		echo "\n\t\t\t<a id=\"logo-container\" href=\"/\" class=\"brand-logo\">Pompeii.net</a>";
		echo "\n\t\t\t\t<ul class=\"right hide-on-med-and-down\">";
		
		navlist($COLOR, $ACTIVE);
		
		echo "\n\t\t\t\t</ul>";
		echo "\n\t\t\t<a data-target=\"nav-mobile\" class=\"sidenav-trigger\"><i class=\"material-icons\">menu</i></a>";
		echo "\n\t\t</div>";
		echo "\n\t</nav>";
		echo "\n</div>";

		echo "\n<!-- Mobile Sidebar Navigator -->";
		echo "\n<ul id=\"nav-mobile\" class=\"sidenav\">";
		
		navlist($COLOR, $ACTIVE);
		
		echo "\n</ul>";
	}
?>
