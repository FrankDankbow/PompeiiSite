<!DOCTYPE html>
<html lang="en">

<!-- Title -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Home / Pompeii.net</title>

	<!-- CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

<body>
	<?php
		require "footer.php";
		require "navbar.php";
		
		$COLOR = "red";
		
		navbar($COLOR, "index.php");
	?>
	
	<div id="index-banner" class="parallax-container row tooltipped" data-position="bottom" data-tooltip="'The Last Day of Pompeii' by Karl Bryullov">
		<div class="section no-pad-bot">
			<div class="container">
				<br><br>
				<h1  class="header center text-lighten-2 white-text">Pompeii.net</h1>
				<div class="row center">
					<h5  class="header col s12 white-text">The Greatest Pompeii Archive In The World</h5>
				</div>
				<div class="row center">
					<a href="about.php" id="download-button" class="btn-large waves-effect waves-light red z-depth-5">About Us</a>
				</div>
				<br><br>
			</div>
		</div>
		<div class="parallax">
			<img src="images/the_last_day_of_pompeii.jpg" alt="'The Last Day of Pompeii' by Karl Bryullov">
		</div>
	</div>

	<div class="container">
		<div class="section">
			<!-- Icon Section -->
			<div class="row">
				<div class="col s12 m4">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons large">library_books</i></h2>
						<h5 class="center">Browse Pompeii.net's Archive Directly</h5>
						<p class="light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id ex vestibulum, volutpat lorem gravida, lacinia massa. Suspendisse sem nibh, interdum ut dolor interdum, lacinia tristique erat. Nunc hendrerit placerat eros eget finibus. Maecenas hendrerit fringilla eros, eu varius diam bibendum et. Proin arcu leo, semper et viverra in, elementum id quam. Pellentesque placerat sagittis mauris, sit amet posuere libero varius ut. Maecenas et tempor sem, quis tincidunt tellus. In eu faucibus libero, eu lacinia elit. Maecenas lacus nisi, commodo sed sapien in, faucibus gravida nunc. </p>
					</div>
				</div>

				<div class="col s12 m4">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons large">search</i></h2>
						<h5 class="center">Search Through Pompeii.net's Archives</h5>
						<p class="light">Cras vitae faucibus arcu. Aenean quis massa tellus. Suspendisse et nulla congue, finibus enim id, suscipit nisl. Duis dignissim commodo neque, eget scelerisque libero interdum at. Donec mattis sodales ornare. Nulla facilisi. Vestibulum tempus tempor felis in posuere. Mauris risus augue, aliquet in ante non, dapibus dignissim sem. Sed eget risus tincidunt, fermentum lectus ac, blandit est. Pellentesque eget dolor eros. Sed vitae ante tortor. Etiam at massa non risus fermentum laoreet id id risus. </p>
					</div>
				</div>

				<div class="col s12 m4">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons large">explore</i></h2>
						<h5 class="center">Learn About Pompeii.net</h5>
						<p class="light">Fusce ornare leo fermentum tellus accumsan, sed fermentum elit sodales. Nulla facilisi. Mauris non purus accumsan risus ultrices tristique vitae in est. Curabitur purus lectus, aliquam et posuere id, convallis sit amet libero. Mauris fermentum faucibus ornare. Suspendisse et risus mauris. Ut quis lectus in dolor pharetra tincidunt vel a libero. Nunc fermentum vitae velit id pulvinar.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php
		footer($COLOR);
	?>
	
	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>
	</body>
</html>
