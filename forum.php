<!DOCTYPE html>
<html lang="en">

<?php
	require "footer.php";
	require "navbar.php";
	
	// CONNECT TO DB 
	$servername = "localhost";
	$username = "pompeii";
	$password = "password";
	$dbname = "PompeiiNet";
	$DBCONNECTION = mysqli_connect($servername, $username, $password, $dbname);
	
	// Check connection
	if (!$DBCONNECTION) {
		die("Connection failed: " . mysqli_connect_error());
	}

	$POSTPERPAGE=10;
	$COLOR = "red"; // Color is the accent color
	$GLOBALS['COLOR'] = $COLOR;
	$BODYCOLOR="white";
	
	// Query the database to get the page count of a scene
	$RESULT = $DBCONNECTION->query("SELECT scene_id, scene_name, CEIL(COUNT(post_id)/$POSTPERPAGE) AS page_count FROM post_table NATURAL JOIN scene_table GROUP BY scene_id");

	function show_scene($SCENETOSHOW, $QUERYRESULT){
		// Resets to beginning
		mysqli_data_seek($QUERYRESULT,0);
		
		while ($ROW = $QUERYRESULT->fetch_assoc()){
			if ($ROW['scene_id'] == $SCENETOSHOW){
				echo "\n<a href=\"/scene/$SCENETOSHOW\" class=\"collection-item avatar waves-effect waves-light z-depth-4\">";
				echo "\n\t<div class=\"row\">";
				echo "\n\t\t<i class=\"secondary-content material-icons circle red\">folder</i>";
				echo "\n\t\t<h6 class=\"col s12\">" . $ROW["scene_name"];
				echo "\n\t\t\t<span class=\"new badge large red\" data-badge-caption=\"Pages\">" . $ROW["page_count"] . "</span>";
				echo "\n\t\t</h6>\n\t</div>\n</a>";
				return;
			}
		}
	}
?>

<!-- Title -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Forum / Pompeii.net</title>

	<!-- CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<!-- link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/ -->
</head>

<?php 
	echo "<body class=\"$BODYCOLOR\">";
	
	navbar($COLOR, "forum.php");
?>
	
	<div id="index-banner" class="parallax-container row tooltipped" data-position="bottom" data-tooltip="'The Last Day of Pompeii' by Karl Bryullov">
		<div class="section no-pad-bot">
			<div class="container">
				<br><br>
				<br><br>
				<h1 class="header center text-lighten-2 white-text">Pompeii Forum</h1>
				<div class="row center">
					<h5 class="header col s12 white-text">Browse Pompeii's Forum</h5>
				</div>
			</div>
		</div>
		<div class="parallax">
			<img src="/images/the_last_day_of_pompeii.jpg" alt="'The Last Day of Pompeii' by Karl Bryullov">
		</div>
	</div>

	<!-- Act 1 -->
	<div class="container">
		<div class="section">	
			<div class="collection with-header">
				<div class="collection-header">
					<h4>ACT ONE</h4>
					<p>Lorem ipsum act 1</p>
				</div>
				
				<?php 
					show_scene(1, $RESULT);
					show_scene(2, $RESULT);
					show_scene(3, $RESULT);
				?>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="section">	
			<div class="collection with-header">
				<div class="collection-header">
					<h4>ACT TWO</h4>
					<p>Lorem ipsum act 2</p>
				</div>
				
				<?php 
					show_scene(4, $RESULT);
					show_scene(5, $RESULT);
					show_scene(6, $RESULT);
					show_scene(7, $RESULT);
				?>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="section">	
			<div class="collection with-header">
				<div class="collection-header">
					<h4>ACT THREE</h4>
					<p>Lorem ipsum act 3</p>
				</div>
				
				<?php 
					show_scene(8, $RESULT);
					show_scene(9, $RESULT);
					show_scene(10, $RESULT);
				?>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="section">	
			<div class="collection with-header">
				<div class="collection-header">
					<h4>ACT FOUR</h4>
					<p>Lorem ipsum act 4</p>
				</div>
				
				<?php 
					show_scene(11, $RESULT);
					show_scene(12, $RESULT);
					show_scene(13, $RESULT);
				?>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="section">
			<div class="collection with-header">
				<div class="collection-header">
					<h4>ACT FIVE</h4>
					<p>Lorem ipsum act 5</p>
				</div>
				
				<?php 
					show_scene(14, $RESULT);
					show_scene(15, $RESULT);
					show_scene(16, $RESULT);
					show_scene(17, $RESULT);
					show_scene(18, $RESULT);
				?>
			</div>
		</div>
	</div>
	
<?php 
	footer($COLOR);

	$RESULT->free_result();
?>

	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="/js/materialize.js"></script>
	<script src="/js/init.js"></script>
</body>
</html>
