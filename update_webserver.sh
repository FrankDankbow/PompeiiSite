#!/bin/bash

# This script puts all the important webserver files in their place
# TODO: use symbolic links for everything?

sudo rm -r /var/www/html/*
sudo cp -r images/ /var/www/html/images
sudo cp -r js/ /var/www/html/js
sudo cp -r css/ /var/www/html/css

sudo cp footer.php /var/www/html/footer.php
sudo cp navbar.php /var/www/html/navbar.php

sudo cp index.php /var/www/html/index.php
sudo cp about.php /var/www/html/about.php
sudo cp forum.php /var/www/html/forum.php
sudo cp search.php /var/www/html/search.php

sudo cp scene.php /var/www/html/scene.php
sudo cp searchfor.php /var/www/html/searchfor.php